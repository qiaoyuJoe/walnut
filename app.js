const Koa = require('koa'),
  router = require('./routers/index.js'),
  cors = require('koa-cors'),
  config = require('./config/default.js'),
  bodyParser = require('koa-bodyparser')

const app = new Koa()
app.use(cors({
  origin: (ctx) => {
    // if (ctx.url.startsWith('/api/v1/')) {

    // }
    return '*'
  },
  maxAge: 5,
  credentials: true,
  allowMethods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
  allowHeaders: ['Content-Type', 'Authorization', 'Accept'],
  exposeHeaders: ['logId']
}))

app.use(bodyParser())
// app.all('*', (req, res, next) => {
//   const { origin, Origin, referer, Referer } = req.headers;
//   const allowOrigin = origin || Origin || referer || Referer || '*';
// 	res.header("Access-Control-Allow-Origin", allowOrigin);
// 	res.header("Access-Control-Allow-Headers", "Content-Type, Authorization, X-Requested-With");
// 	res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
//   res.header("Access-Control-Allow-Credentials", true); //可以带cookies
// 	res.header("X-Powered-By", 'Express');
// 	if (req.method == 'OPTIONS') {
//   	res.sendStatus(200);
// 	} else {
//     next();
// 	}
// });



router(app)

app.listen(config.port, () => {
  console.log('监听了端口', config.port)
})