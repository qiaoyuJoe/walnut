const verificationCode = () => {
  var sum = 0;  //记录出现合格条件的数字的次数
  var str = "";
  while (1){
    var num = Math.round(Math.random()*122);
    if (num >= 0 && num <= 9){
        sum ++;
        str += num;
    }else if (num >= 97 && num <= 122){
        sum++;
        str += String.fromCharCode(num);
    }else if(num >= 65 && num <= 90){
        sum++;
        str+=String.fromCharCode(num);
    }else {
        //不符合验证码的规则
    }
    if (sum == 4) {
        break;
    }
  }
  return str
}

module.exports = {
  verificationCode
}
