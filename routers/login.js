const router = require('koa-router')({
  prefix: '/api/v1'
})
const LoginController = require('../controllers/login')

router.post('/walnut/login', LoginController.create)
router.get('/walnut/:uname', LoginController.detail)
router.post('/walnut/setEmail', LoginController.sendEmailVerificationCode)

module.exports = router