const loginRoute = require('./login')


module.exports = (app) => {
  app.use(loginRoute.routes()).use(loginRoute.allowedMethods())
}