const db = require('../config/db')
const moment = require('moment')

const Sequelize = db.sequelize

// 导入表的解构， 附建创建、写入、查询
const U = Sequelize.import('../schema/utils.js')
U.sync({force: false}) // 自动创建表

class UModel {
  // 创建登录模型
  static async createCode({ email, code }) {
    return await U.create({ 
      email: email, 
      code: code
    })
  }

  // 查询 同名用户
  static async getCode(email) {
    return await U.findOne({
      where: {
        email
      }
    })
  }
}
module.exports = UModel