const db = require('../config/db')

const Sequelize = db.sequelize

// 导入表的解构， 附建创建、写入、查询
const Login = Sequelize.import('../schema/login.js')
Login.sync({force: false}) // 自动创建表

class LoginModel {
  // 创建登录模型
  static async createLogin(data) {
    return await Login.create({
      uname: data.uname,
      pword: data.pword
    })
  }

  // 查询 同名用户
  static async getUser(uname) {
    return await Login.findOne({
      where: {
        uname
      }
    })
  }
  // 查询所有的用户
  static async getAllUser() {
    return await Login.findAll()
  }
}
module.exports = LoginModel