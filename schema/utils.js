const moment = require('moment')
module.exports = (sequelize, DataTypes) => {
  return sequelize.define('utils', {
    email: {
      type: DataTypes.STRING,
      primaryKey: true,
      allowNull: true,
      // autoIncrement: true
    },
    code: DataTypes.STRING,
    // create_time: {
    //   type: DataTypes.TIMESTAMP,
    //   allowNull: false,
    //   get() {
    //     return moment().format('YYYY-MM-DD HH:mm:ss')
    //   }
    // },
  }, {
    freezeTableName: true,
    timestamps: false
  })
}