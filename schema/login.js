const moment =require('moment')
module.exports = (sequelize, DataTypes) => {
  return sequelize.define('walnut', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: true,
      autoIncrement: true
    },
    uname: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'uname'
    },
    pword: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'pword'
    }
  }, {
    freezeTableName: true,
    timestamps: false
  })
}