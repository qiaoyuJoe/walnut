// module.exports = {
//   root: true,
//   env: {
//     node: true
//   },
//   // extends: [
//   //   'plugin:vue/essential',
//   //   '@vue/airbnb'
//   // ],
//   // parserOptions: {
//   //   parser: 'babel-eslint'
//   // },
//   // settings: {
//   //   'import/resolver': {
//   //     webpack: {
//   //       config: './node_modules/@vue/cli-service/webpack.config.js'
//   //     }
//   //   }
//   // },
//   rules: {
//     'linebreak-style': [0 , 'error', 'windows'],
//     'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
//     semi: 'off',
//     'comma-dangle': ['error', 'never'],
//     'max-len': ['error', 400],
//     'no-underscore-dangle': ['off'], //变量名下划线
//     'no-param-reassign': ['off'], // 参数赋值
//     'array-callback-return':['off'], // 数组操作返回值
//     'global-require': ['off'],
//     'no-empty':['off'],
//     'no-plusplus': ['off'],
//     'no-unused-expressions': [ 'off', {
//       allowTernary: true
//     }],
//     "import/no-unresolved": [0],
//     'no-script-url': 0,
//     "object-curly-newline": 'off',
//     "import/no-cycle": 'off',
//     "consistent-return": 'off',
//     'camelcase': 'off',
//     'no-console': 'off',
//     'no-restricted-syntax': "off",
//     'arrow-body-style': 'off',
//     'import/extensions': ['off']
//   },
//   overrides: [
//     {
//       files: [
//         '**/__tests__/*.{j,t}s?(x)',
//         '**/tests/unit/**/*.spec.{j,t}s?(x)'
//       ],
//       env: {
//         jest: true
//       }
//     }
//   ]
// };
