const LoginModel = require('../modules/login')
const nodemailer = require('nodemailer')
const utils = require('../utils/index')
const UModel = require('../modules/utils')
class LoginController {
  // 创建用户
  static async create(ctx) {
    let req = ctx.request.body
    if (req.uname && req.pword) {
      try {
        // 创建
        const ret = await LoginModel.createLogin(req)
        // 查询
        const data = await LoginModel.getUser(ret.uname)

        ctx.response.status = 200
        ctx.body = {
          code: 200,
          msg: '创建成功',
          data
        }
      } catch (error) {
        ctx.response.status = 412
        ctx.body = {
          code: 412,
          msg: '创建失败'
        }
      }
    } else {
      ctx.response.status = 416
      ctx.body = {
        code: 200,
        msg: '创建失败'
      }
    }
  }

  static async detail(ctx) {
    let uname = ctx.params.uname
    if (uname) {
      try {
        let data = await LoginModel.getUser(uname)
        ctx.response.status = 200
        ctx.body = {
          code :200,
          msg: '查询成功',
          data
        }
      } catch (error) {
        ctx.response.status = 412
        ctx.body = {
          code: 412,
          msg: '查询失败'
        }
      }
    } else {
      ctx.response.status = 416
      ctx.body = {
        code: 200,
        msg: '查询失败'
      }
    }
  }
  static async sendEmailVerificationCode (ctx) {
      const { email } = ctx.request.body
      const code = utils.verificationCode() // 生成验证码
      UModel.createCode({ email, code })
      LoginController.sendEmailToUser({
        title: '西红柿-验证邮件',
        email,
        content: '<h1 style="text-align: center">西红柿系统</h1>' +
        '<h2 style="font-size: 16px">验证码<h2/>' +
        '<p>尊敬的用户：</p>' +
        '<p>你的邮箱验证码为<u><b>' + code + '</b></u>，请在网页中填写，完成验证。</p>' +
        '<a href="javascript:;" style="color: #1890ff">西红柿系统</a>'
      })
      ctx.body = {
        code: 200,
        msg: '发送成功'
      }

  }
  static async sendEmailToUser({ title , email, content }) {
      let transporter = nodemailer.createTransport({
        host: 'smtp.exmail.qq.com',
        port: 465,
        secure: true,
        auth: {
          user: 'qiaoyu@zmeng123.com',
          pass: '123456aA.'
        }
      })
      let mailOptions = {
        from: 'qiaoyu@zmeng123.com',
        to: email,
        subject: title,
        html: content
      }
      transporter.sendMail(mailOptions, (e, info) => {})
  }
}

module.exports = LoginController